<?php

namespace Adamek\ProductInfo\Filters;

use Magento\Catalog\Api\Data\ProductSearchResultsInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

class ProductFilter
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @param ProductRepositoryInterface $productRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * Gets search results for applied criteria.
     *
     * @param string $fieldName
     * @param string $fieldValue
     * @param string $filterType
     *
     * @return ProductSearchResultsInterface
     */
    public function getProducts($fieldName, $fieldValue, $filterType): ProductSearchResultsInterface
    {
        $searchCriteria = $this
            ->searchCriteriaBuilder
            ->addFilter($fieldName, $fieldValue, $filterType)
            ->create()
        ;

        return $products = $this
            ->productRepository
            ->getList($searchCriteria)
        ;
    }
}
