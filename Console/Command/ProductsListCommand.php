<?php

namespace Adamek\ProductInfo\Console\Command;

use Adamek\ProductInfo\Filters\ProductFilter;
use Adamek\ProductInfo\Formatters\FormatterBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ProductsListCommand extends Command
{
    /**
     * @var string Command name
     */
    const COMMAND_NAME = 'adamek:product:list';

    /**
     * @var string Field long option name
     */
    const FIELD_OPTION_NAME = 'field';

    /**
     * @var string Value long option name
     */
    const VALUE_OPTION_NAME = 'value';

    /**
     * @var string Comparator long option name
     */
    const COMPARATOR_OPTION_NAME = 'comparator';

    /**
     * @var string Format long option name
     */
    const FORMAT_OPTION_NAME = 'format';

    /**
     * Allowed comparators:
     *   - eq => equal
     *   - neq => not equal
     *   - like => like
     *   - nlike => not like
     *   - is => is
     *   - moreq => more or equal
     *   - gt => greater
     *   - lt => less
     *   - gteq => greater or equal
     *   - lteq => less or equal
     *   - from => from
     *   - to => to
     * @var string[]
     */
    const ALLOWED_COMPARATORS = ['eq', 'neq', 'like', 'nlike', 'is', 'moreq', 'gt', 'lt', 'gteq', 'lteq', 'from', 'to'];

    /**
     * @var ProductFilter $pf
     */
    protected $pf;

    /**
     * @var FormatterBuilder $builder
     */
    protected $builder;

    /**
     * @param ProductFilter $productFilter
     * @param FormatterBuilder $formatterBuilder
     */
    public function __construct(
        ProductFilter $productFilter,
        FormatterBuilder $formatterBuilder
    )
    {
        $this->pf = $productFilter;
        $this->builder = $formatterBuilder;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('Generates a list of products meeting selected search criteria. List format can be specified')
            ->setDefinition([
                new InputOption(
                    self::FIELD_OPTION_NAME,
                    'f',
                    InputOption::VALUE_OPTIONAL,
                    'Name of a product field to search in, e.g. price, sku, etc.',
                    'price'
                ),
                new InputOption(
                    self::VALUE_OPTION_NAME,
                    'l',
                    InputOption::VALUE_OPTIONAL,
                    'Value to search for.',
                    500
                ),
                new InputOption(
                    self::COMPARATOR_OPTION_NAME,
                    'c',
                    InputOption::VALUE_OPTIONAL,
                    sprintf('Comparison operator. Supported: [%s].', implode(', ', self::ALLOWED_COMPARATORS)),
                    'lt'
                ),
                new InputOption(
                    self::FORMAT_OPTION_NAME,
                    '',
                    InputOption::VALUE_OPTIONAL,
                    'List output format. Supported: [json].',
                    'json'
                )
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $field = $input->getOption(self::FIELD_OPTION_NAME);
        $value = $input->getOption(self::VALUE_OPTION_NAME);
        $comparator = $input->getOption(self::COMPARATOR_OPTION_NAME);
        $format = $input->getOption(self::FORMAT_OPTION_NAME);

        $output->writeln(sprintf('List type: %s; Selection criteria: %s %s %s', $format, $field, $comparator, $value));

        $formatter = $this->builder->build($format);
        $searchResult = $this->pf->getProducts($field, $value, $comparator);
        $formattedResult = $formatter->format($searchResult);

        $output->writeln($formattedResult);

        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }
}
