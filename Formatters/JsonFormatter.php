<?php

namespace Adamek\ProductInfo\Formatters;

use Magento\Catalog\Api\Data\ProductSearchResultsInterface;

class JsonFormatter implements FormatterInterface
{
    public function format(ProductSearchResultsInterface $searchResult): string
    {
        $result = [];
        $products = $searchResult->getItems();

        foreach ($products as $product) {
            $result[] = [
                'name' => $product->getName(),
                'sku' => $product->getSku(),
                'price' => $product->getPrice(),
            ];
        }

        return json_encode($result);
    }
}
