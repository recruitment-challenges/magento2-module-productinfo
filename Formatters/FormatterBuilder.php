<?php

namespace Adamek\ProductInfo\Formatters;

class FormatterBuilder
{
    /**
     * Builds appropriate formatter based on requested type.
     *
     * @param string $type Formatter type
     */
    public static function build(string $type): FormatterInterface
    {
        $className = sprintf('%s\%sFormatter', __NAMESPACE__, ucfirst(strtolower($type)));

        if (!class_exists($className)) {
            throw new \OutOfBoundsException(sprintf('Requested formatter "%s" does not exists', $className));
        }

        return new $className();
    }
}
