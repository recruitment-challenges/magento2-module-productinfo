<?php

namespace Adamek\ProductInfo\Formatters;

use Magento\Catalog\Api\Data\ProductSearchResultsInterface;

interface FormatterInterface
{
    /**
     * Format provided search result into expected string like json string, xml, etc.
     */
    public function format(ProductSearchResultsInterface $searchResult): string;
}
