# Magento2 Module Product Info

Magento2 Module with CLI Product Info Tool to get list of products by defined criteria.

## How to install
```bash
# go to Magento2 installation directory
cd /path/to/magento

# add git repository
composer config repositories.adamek-magento git git@gitlab.com:recruitment-challenges/magento2-module-productinfo.git

# add new module
composer require adamek/module-productinfo dev-master

# enable module
./bin/magento module:enable Adamek_ProductInfo

# build dependencies
./bin/magento setup:di:compile

# clear cache
./bin/magento cache:clean
```

## How to use it
To get list of products execute console command and provide expected search criteria:
```bash
./bin/magento adamek:product:list --field=price --comparator=to --value=1500
```
If you skip criteria, default one will be in place -> products with price lower then 500.

### Output format
Currently only JSON is supported.

### Comparators
You can use below comparators:
- eq => equal
- neq => not equal
- like => like
- nlike => not like
- is => is
- moreq => more or equal
- gt => greater
- lt => less
- gteq => greater or equal
- lteq => less or equal
- from => from
- to => to

### Fields
You can use any fields that are available in Magento2. Check https://devdocs.magento.com/guides/v2.4/graphql/interfaces/product-interface.html to find out more.
